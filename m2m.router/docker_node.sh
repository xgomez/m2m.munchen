!#/bin/bash

alias n='docker run -it --rm \
  -v %cd%:/usr/src/m2m.router \
  -w /usr/src/m2m.router \
  node:8.11.2-alpine'

n npm init
