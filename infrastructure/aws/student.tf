# These are variables that you must fill in before you can deploy infrastructure.

variable "student_id" {
  description = "A unique ID that separates your resources from everyone else's"
  default     = "1"
}

variable "aws_region" {
  description = "The AWS Region to use"
  default     = "eu-central-1"
}

variable "database_username" {
  description = "Your chosen master user name for the database. Must be less than 16 characters and may only use a-z, A-Z, 0-9, '$' and '_'. The first character cannot be a digit."
  default     = "m2mrepo"
}

variable "database_password" {
  description = "Your chosen master user password for the database. Must be at least 8 characters, but not contain '/', '\\', \", or '@'."
  default     = "m2mreposecret"
}

variable "public_key" {
  description = "The public half of your SSH key. Make sure it is in OpenSSH format (it should start with 'ssh', not 'BEGIN PUBLIC KEY')"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDcirKUznTBAo70zj/Yzt4LLz+bgQzjGzXLLTs6cMkQS2Hutw0rHuefCJBfsA/yhYyjD3AtNQz+b0YojgTJaxolOSa4zfM1h8u+QTADqe3QbfhJSc5k27vphzEj4hN5ac+c+Oi62vJnuQUx+D42kds9SKV6WkEq9O8vdYq5avDam43N2idk3ax7bqZWslJCqSEViWfTl97jsBnpYjh/WuZ4HtLJ4XaEzREJiKKYsPNUCr4vmG1FbwgTyXj30msym7E6CjlDQvJyZvd97KqRHWvOJ4bUkWFfdYJdRkNj657g6V9j9fBvvO3GrH/8gYl2PJwEQB29W+r/sQRQlX/FiW6Z xgomez@xgomez.com"
}

variable "s3_bucket" {
  description = "This is your personal S3 bucket name"
  default = "m2mxgomez"
}

variable "aws_access_key_id" {
  description = "This is the access key ID of an IAM user that can read from your S3 bucket"
  default = "AKIAJS63S6N66URFFD6Q"
}

variable "aws_secret_access_key" {
  description = "This is the secret access key of the same IAM user, one who can read from your S3 bucket"
  default = "cc/bW9HeZzmINCLGm9pUdyvs95jw0UwZyMXmkz8M"
}
