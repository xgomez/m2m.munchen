#cloud-config
write_files:
-   encoding: b64
    path: /etc/telegraf/telegraf.d/telegraf.conf
    content: "${telegraf_conf_content}"
    permissions: '0644'

package_upgrade: false

packages:
  - awscli
  - nodejs

runcmd:
  - wget https://dl.influxdata.com/telegraf/releases/telegraf_1.6.3-1_amd64.deb
  - sudo dpkg -i telegraf_1.6.3-1_amd64.deb
  - export ZIPKIN_SERVICE_URL=${zipkin_service_url}
  - mkdir -p /home/ubuntu/app
  - AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${router_service_bucket}/${router_service_tgz}" /home/ubuntu/${router_service_tgz}
  - sudo chown -R ubuntu:ubuntu /home/ubuntu/app
  - sudo chown -R ubuntu:ubuntu /home/ubuntu/${router_service_tgz}
  - su -l -c "tar xzvf /home/ubuntu/${router_service_tgz} -C /home/ubuntu/app" ubuntu
  - su -l -c "LOBSTERS_DNS_NAME='${lobsters_dns_name}' MODLOG_DNS_NAME='${modlog_dns_name}' node /home/ubuntu/app/index.js" ubuntu